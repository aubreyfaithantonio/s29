// Sample Data

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "654789123",
			email: "stephenhawking@email.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "HR"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "789456321",
			email: "neilarmstrong@email.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "HR"
	},
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "321456987",
			email: "janedoe@email.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "HR"
	}
])

// Query Operators
//  $or operator 

// 1st option
// db.users.find({
// 	$or: [
// 		{
// 			firstName: { $regex: 'S' }
// 		},
// 		{
// 			lastName: { $regex: 'D' }
// 		},
// 	]
// },
// 	{
// 		_id: 0,
// 		age: 0,
// 		contact: 0,
// 		department: 0,
// 		courses: 0
// 	}
// )

// 2nd option
db.users.find({
	$or: [
		{firstName:{$regex: 's', $options: "$i"}},
		{lastName:{$regex: 'd', $options: "$i"}},
	]
},
	{
		firstName: 1,
		lastName: 1,
		_id: 0,
	}
)
//  $or operator END

// $and operator
db.users.find({
	$and: [
		{
			department: "HR"
		},
		{
			age: { $gte: 70}
		}
	]
})
// $and operator END

// $and, $regex, $lte
db.users.find({
	$and: [
		{
			firstName: 
			{ 
				$regex: 'e',
				$options: "$i"
			}
		},
		{
			age: { $lte: 30}
		},
	]
})
// $and, $regex, $lte END

// Query Operators END